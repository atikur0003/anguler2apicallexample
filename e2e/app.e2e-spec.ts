import { Anguler2ApiCallExamplePage } from './app.po';

describe('anguler2-api-call-example App', () => {
  let page: Anguler2ApiCallExamplePage;

  beforeEach(() => {
    page = new Anguler2ApiCallExamplePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
